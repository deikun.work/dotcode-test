package dotcode.backend.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import dotcode.backend.model.User;
import dotcode.backend.repository.UserRepository;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

    protected static MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeAll
    static void beforeAll(@Autowired WebApplicationContext applicationContext) {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .build();
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void findAll_WhenTwoUsersInDb_ShouldReturnPageWithTwoUsers() throws Exception {
        User user1 = User.builder()
                .firstName("John")
                .lastName("Doe")
                .age(18)
                .build();

        User user2 = User.builder()
                .firstName("Alice")
                .lastName("Johnson")
                .age(25)
                .build();

        List<User> userList = List.of(user1, user2);

        Page<User> userPage = new PageImpl<>(userList);

        userRepository.saveAll(userPage);

        mockMvc.perform(get("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(2)));
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void findById_WhenUserExist_ShouldReturnUser() throws Exception {
        User user = User.builder()
                .firstName("John")
                .lastName("Doe")
                .age(18)
                .build();

        userRepository.save(user);

        MvcResult result = mockMvc.perform(get("/api/v1/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        User actual = objectMapper.readValue(result.getResponse().getContentAsString(), User.class);

        Assertions.assertNotNull(actual);
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void findById_WhenUserDoesNotExist_ShouldThrowException() throws Exception {
        mockMvc.perform(get("/api/v1/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void save_WithValidData_ShouldSaveUserAndReturnCreatedStatus() throws Exception {
        User user = User.builder()
                .firstName("John")
                .lastName("Doe")
                .age(18)
                .build();

        String jsonRequest = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest))
                .andExpect(status().isCreated());
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void save_WithNegativeAge_ShouldReturnBadRequestStatus() throws Exception {
        User user = User.builder()
                .firstName("John")
                .lastName("Doe")
                .age(-1)
                .build();

        String jsonRequest = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void save_WithAgeBiggerThan200_ShouldReturnBadRequestStatus() throws Exception {
        User user = User.builder()
                .firstName("John")
                .lastName("Doe")
                .age(201)
                .build();

        String jsonRequest = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void save_WithBlankFirstName_ShouldReturnBadRequestStatus() throws Exception {
        User user = User.builder()
                .firstName("")
                .lastName("Doe")
                .age(18)
                .build();

        String jsonRequest = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void save_WithBlankLastName_ShouldReturnBadRequestStatus() throws Exception {
        User user = User.builder()
                .firstName("John")
                .lastName("")
                .age(18)
                .build();

        String jsonRequest = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void delete_ShouldReturnNoContentStatusAndDeleteUserFromDb() throws Exception {
        User user = User.builder()
                .firstName("John")
                .lastName("Doe")
                .age(18)
                .build();

        userRepository.save(user);

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/users/" + user.getId()))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/api/v1/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(0)));

    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void update_WhenUserExist_ShouldReturn200Status() throws Exception {
        User user = User.builder()
                .firstName("John")
                .lastName("Doe")
                .age(18)
                .build();

        User save = userRepository.save(user);

        User expected = User.builder()
                .firstName("Alice")
                .lastName("Johnson")
                .age(25)
                .build();

        String jsonRequest = objectMapper.writeValueAsString(expected);

        MvcResult result = mockMvc.perform(patch("/api/v1/users/" + save.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest))
                .andExpect(status().isOk())
                .andReturn();

        User actual = objectMapper.readValue(result.getResponse().getContentAsString(), User.class);

        assertEquals(actual.getFirstName(), expected.getFirstName());
        assertEquals(actual.getLastName(), expected.getLastName());
        assertEquals(actual.getAge(), expected.getAge());
    }

    @Test
    @Sql(scripts = "classpath:db/clear-users-table.sql",
            executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void update_WhenUserDoesNotExist_ShouldReturn404NotFound() throws Exception {
        User user = User.builder()
                .firstName("Alice")
                .lastName("Johnson")
                .age(25)
                .build();

        String jsonRequest = objectMapper.writeValueAsString(user);

        mockMvc.perform(patch("/api/v1/users/999")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonRequest))
                .andExpect(status().isNotFound());
    }
}