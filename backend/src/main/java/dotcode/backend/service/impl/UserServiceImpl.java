package dotcode.backend.service.impl;

import dotcode.backend.dto.UserDto;
import dotcode.backend.exception.UserNotFoundException;
import dotcode.backend.model.User;
import dotcode.backend.repository.UserRepository;
import dotcode.backend.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User save(UserDto request) {
        User user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .age(request.getAge())
                .build();
        return userRepository.save(user);
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public User findById(Long id) {
        return userRepository
                .findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    @Modifying
    public User update(Long id, UserDto updateRequest) {
        User user = userRepository
                .findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));

        user.setAge(updateRequest.getAge());
        user.setLastName(updateRequest.getLastName());
        user.setFirstName(updateRequest.getFirstName());
        return userRepository.save(user);
    }
}
