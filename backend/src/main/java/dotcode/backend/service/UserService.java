package dotcode.backend.service;

import dotcode.backend.dto.UserDto;
import dotcode.backend.model.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    User save(UserDto user);

    Page<User> findAll(Pageable pageable);

    User findById(Long id);

    void deleteById(Long id);

    User update(Long id, UserDto user);

}
