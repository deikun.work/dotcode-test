package dotcode.backend.exception;

public class UserNotFoundException extends RuntimeException {
    private static final String ERROR_MESSAGE = "User with id %d not found";

    public UserNotFoundException(Long id) {
        super(ERROR_MESSAGE.formatted(id));
    }
}