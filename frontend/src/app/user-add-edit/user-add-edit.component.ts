import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CoreService} from "../core/core.service";

@Component({
  selector: 'app-user-add-edit',
  templateUrl: './user-add-edit.component.html',
  styleUrl: './user-add-edit.component.css'
})
export class UserAddEditComponent implements OnInit {
  userForm: FormGroup;


  constructor(private _fb:FormBuilder,
              private _userService: UserService,
              private _dialogRef: MatDialogRef<UserAddEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private _coreService: CoreService,
  ) {
    this.userForm = this._fb.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100), Validators.pattern('[a-zA-Z]*(\'[a-zA-Z]*)?')]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100), Validators.pattern('[a-zA-Z]*(\'[a-zA-Z]*)?')]],
      age: ['', [Validators.required, Validators.min(0), Validators.max(200)]]
    })
  }

  ngOnInit(): void {
    this.userForm.patchValue(this.data);
  }

  onFormSubmit() {
    if (this.userForm.valid) {
      if (this.data) {
        this._userService.updatedUser(this.data.id, this.userForm.value)
          .subscribe({
            next: (val: any) => {
              this._dialogRef.close(true);
              this._coreService.openSnackBar('User details successfully updated')
            },
            error: (err: any) => {
              console.error(err);
            }
          })
      } else {
        this._userService.addUser(this.userForm.value).subscribe({
          next: (val: any) => {
            this._dialogRef.close(true);
            this._coreService.openSnackBar('User successfully added')
          },
          error: (err: any) => {
            console.error(err);
          }
        })
      }
    }
  }
}
