import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http:HttpClient) { }

  addUser(data: any): Observable<any> {
    return this._http.post('http://localhost:8080/api/v1/users', data)
  }

  updatedUser(id: number, data: any): Observable<any> {
    return this._http.patch(`http://localhost:8080/api/v1/users/${id}`, data)
  }

  getUsersList(): Observable<any> {
    return this._http.get('http://localhost:8080/api/v1/users')
  }

  deleteUser(id: number): Observable<any> {
    return this._http.delete(`http://localhost:8080/api/v1/users/${id}`)
  }
}
